import requests
from pymongo import MongoClient
import pandas as pd
import time
from lxml import etree
from fake_useragent import UserAgent
import json

years = range(1996,2018)
game_types = [1,2,4]
for year in years:
    for game_type in game_types:
        url = 'http://china.nba.com/static/data/league/teamstats_All_All_' + str(year) + '_' + str(game_type)+'.json'
        ua = UserAgent()
        headers = {}
        headers['User-Agent'] = ua.random
        r = requests.get(url,headers = headers)
        data_payload = r.json()['payload']
        print(data_payload)
        filename = str(year) + '年' +str(game_type) + '比赛数据.json'
        with open(filename, 'a') as f:
            json.dump(data_payload, f)