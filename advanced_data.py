import requests
from fake_useragent import UserAgent
import json
import time

times = range(1996,2018)
game_types=[1,2,4]
gesture_types = ['offensive','defensive','gameInfractions',]
data_types = ['total','perGame','per48','paceAdjusted','paceAdjustedPer48']
for time in times:
    for game_type in game_types:
        for gesture_type in gesture_types:
            for data_type in data_types:
                url = 'http://china.nba.com/static/data/league/advancedteamstats_' \
                      + gesture_type + '_All_' + data_type + '_' + str(time) + '_' + str(game_type) + '.json'
                ua = UserAgent()
                headers = {}
                headers['User-Agent'] = ua.random
                r = requests.get(url, headers=headers)
                data = r.json()['payload']
                print(str(time) + '，比赛类型：'+ str(game_type) + '，动作类型：' + gesture_type + ',技术类型:'+ data_type)

                filename = str(time) + '_' + str(game_type) + '_' + gesture_type + '_' + data_type + '.json'
                with open(filename, 'w') as f:
                    json.dump(data, f)