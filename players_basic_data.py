import requests
from pymongo import MongoClient
from fake_useragent import UserAgent
import json

# 建立数据库
client = MongoClient()   #新建数据库实例
db = client.players        #新建group数据库
all_players = db.all_players_basic_data  #新建Season_data_pg表格

def get_all_players_basic_data():
    url = 'http://china.nba.com/static/data/league/playerlist.json'
    ua = UserAgent()
    # 请求头
    headers={}
    headers['User-Agent'] = ua.random
    # 获取数据
    r = requests.get(url,headers = headers)
    # 打印payload和seasons中的数据
    data = r.json()['payload']['players']
    print(data)
    # all_players.insert(data)
    # 设置文件名
    filename = '505位球员基本信息ture.json'
    # 写入文件
    with open(filename, 'w') as f:
        json.dump(data, f)
    all_players.insert(data)
if __name__ == '__main__':
    get_all_players_basic_data()