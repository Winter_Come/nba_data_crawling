import json
import pymysql

teams = ['clippers', 'lakers', 'kings', 'warriors', 'suns', 'grizzlies', 'pelicans', 'mavericks', 'rockets', 'spurs',
            'jazz', 'nuggets', 'thunder', 'timberwolves', 'blazers', '76ers', 'celtics', 'knicks', 'nets', 'raptors',
            'hornets', 'hawks', 'heat', 'magic', 'wizards', 'bucks', 'bulls', 'cavaliers', 'pacers', 'pistons']
for team in teams:
    with open(team + '.json') as f:
        contents = json.loads(f.read())
        datas = contents['0']
        a = [datas.get('0'),datas.get('1'),datas.get('2'),datas.get('3'),datas.get('4'),datas.get('5'),team]
        print(a)
        conn = pymysql.connect(host="localhost", user="root",
                               password="root", db="nba", port=3306, charset='utf8')
        # 使用cursor()方法获取操作游标
        cur = conn.cursor()
        # 编写sql语句
        sql = "INSERT INTO team_info(teamName,enterNba,home,url,coach,introduce,teamCode) VALUES (%s,%s,%s,%s,%s,%s,%s)"
        effect = cur.execute(sql, a)
        conn.commit()
    print('添加数据库成功！')
    # 关闭游标
    cur.close()
    # 关闭连接
    conn.close()