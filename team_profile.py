# -*- encoding='utf-8' -*-
# 本python代码  获取所有NBA球队基础信息，
# 包括进入NBA时间、主场、分区、官网、主教练、简介等信息
import pymysql
import requests
from pymongo import MongoClient
import pandas as pd
import time
from lxml import etree
from fake_useragent import UserAgent
import json


def get_all_team_profile():
    #所有队伍列表(注意：76人的名称与NBA数据网名称不一致：76ers)
    teams = ['clippers', 'lakers', 'kings', 'warriors', 'suns', 'grizzlies', 'pelicans', 'mavericks', 'rockets', 'spurs',
            'jazz', 'nuggets', 'thunder', 'timberwolves', 'blazers', '76ers', 'celtics', 'knicks', 'nets', 'raptors',
            'hornets', 'hawks', 'heat', 'magic', 'wizards', 'bucks', 'bulls', 'cavaliers', 'pacers', 'pistons']
    #遍历所有队伍的相关信息网页
    for team in teams:
        ua = UserAgent()
        headers = {}
        headers['User-Agent'] = ua.random
        url = 'https://nba.hupu.com/teams/' + team
        r = requests.get(url,headers = headers).text
        # 进行HTML解析
        s = etree.HTML(r)
        # 使用Xpath进行爬取信息定位
        datas = s.xpath('//div[@class="team_data"]/h2/span[1]/text() | '
                       '//div[@class="font"]/p[1]/text()|'
                       '//div[@class="font"]/p[2]/text()|'
                       '//div[@class="font"]/p[3]/a/text()|'
                       '//div[@class="font"]/p[4]/text()|'
                       '//div[@class="txt"]/text()')
        #生成json文件
        df = pd.DataFrame(datas)
        df.to_json(team + '.json')

        #连接数据库
    #     conn = pymysql.connect(host="localhost", user="root",
    #                          password="root", db="nba", port=3306,charset = 'utf8')
    #     # 使用cursor()方法获取操作游标
    #     cur = conn.cursor()
    #     #编写sql语句
    #     # (name,code,city)
    #     sql = "INSERT INTO test(name,code,city) VALUES (%s,%s,%s)"
    #     effect = cur.execute(sql,newsObjs)
    #     conn.commit()
    # # 关闭游标
    # cur.close()
    # # 关闭连接
    # conn.close()





        # df = pd.DataFrame(data)
        # df.to_json(team + '.json')
                    # filename='30球队基础信息.json'
                    # with open(filename,'a') as f:
                    #     json.dump(data,f,ensure_ascii=False)
                    # print('爬取球队：' + team + '基础数据成功！')
if __name__ == '__main__':
    get_all_team_profile()