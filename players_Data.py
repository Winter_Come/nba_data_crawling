import requests
import time
from fake_useragent import UserAgent
import json

years = 2012
#季前赛13的可以，12的不行
game_types = [1]
for year in years:
    for game_type in game_types:
        url = 'http://china.nba.com/static/data/league/playerstats_All_All_All_0_All_false_' + str(year) + '_'+ str(game_type) + '_All_Team_points_All_perGame.json'
        ua = UserAgent()
        headers = {}
        headers['User-Agent'] = ua.random
        r = requests.get(url,headers = headers)
        data_payload = r.json()['payload']
        filename = '球员' + str(year) + '年' +str(game_type) + '类型数据.json'
        with open(filename, 'a') as f:
            json.dump(data_payload, f)
        print(str(year) + '年类型' + str(game_type) + '爬取成功')