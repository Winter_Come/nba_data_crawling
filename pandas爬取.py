import pandas as pd
import html5lib
data = pd.DataFrame()
url_list = ['http://www.espn.com/nba/salaries/_/seasontype/4']
for i in range(2, 13):
    url = 'http://www.espn.com/nba/salaries/_/page/%s/seasontype/4' % i
    url_list.append(url)
for url in url_list:
    data = data.append(pd.read_html(url), ignore_index=True)
data = data[[x.startswith('$') for x in data[3]]]
print(data)
data.to_json('salary.json',orient='table')